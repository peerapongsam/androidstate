package com.peerapongsam.android.state.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [BundleEntity::class], version = 1)
abstract class BundleDatabase : RoomDatabase() {

    companion object {
        fun database(context: Context, name: String): BundleDatabase {
            return Room.databaseBuilder(context, BundleDatabase::class.java, name)
                    .build()
        }
    }

    abstract fun bundleDao(): BundleDao
}