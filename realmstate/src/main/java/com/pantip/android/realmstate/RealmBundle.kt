package com.pantip.android.realmstate

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * @author peerapongsam
 */
open class RealmBundle(@PrimaryKey var id: String = "",
                       var value: String = "") : RealmObject()