package com.pantip.prefstate.example.topic

import com.pantip.prefstate.example.mvp.BaseContract

/**
 * @author peerapongsam
 */
interface TopicContract {
    interface View : BaseContract.View<TopicViewModel>

    interface Presenter : BaseContract.Presenter<TopicViewModel, View> {
        fun fetchTopics()
    }
}