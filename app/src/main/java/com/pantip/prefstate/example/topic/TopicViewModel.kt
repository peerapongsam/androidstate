package com.pantip.prefstate.example.topic

import org.parceler.Parcel
import org.parceler.ParcelConstructor

/**
 * @author peerapongsam
 */
@Parcel(Parcel.Serialization.BEAN)
data class TopicViewModel @ParcelConstructor constructor(val loading: Boolean,
                                                         val items: List<TopicItemViewModel>)

@Parcel(Parcel.Serialization.BEAN)
data class TopicItemViewModel @ParcelConstructor constructor(val id: Int,
                                                             val title: String,
                                                             val description: String,
                                                             val imageCover: String,
                                                             val author: String,
                                                             val avatar: String,
                                                             val comments: Int)