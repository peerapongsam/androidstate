package com.pantip.android.realmstate

import android.content.Context
import android.os.Bundle

/**
 * @author peerapongsam
 */
object RealmState {

    private var realmStateDelegate: RealmStateDelegate? = null

    private fun checkInitialization() {
        if (realmStateDelegate == null) {
            throw IllegalStateException(
                    "You must first call initialize before calling any other methods")
        }
    }

    fun clear(target: Any) {
        checkInitialization()
        realmStateDelegate!!.clear(target)
    }

    fun clearAll(context: Context) {
        val delegate = if (realmStateDelegate != null)
            realmStateDelegate
        else
            RealmStateDelegate(context)

        delegate?.clearAll()
    }

    fun initialize(context: Context) {
        realmStateDelegate = RealmStateDelegate(context)
    }

    fun restoreInstanceState(target: Any, state: Bundle?): Bundle? {
        checkInitialization()
        return realmStateDelegate?.restoreInstanceState(target, state)
    }

    fun saveInstanceState(target: Any, bundle: Bundle, outState: Bundle) {
        checkInitialization()
        realmStateDelegate?.saveInstanceState(target, bundle, outState)
    }
}
