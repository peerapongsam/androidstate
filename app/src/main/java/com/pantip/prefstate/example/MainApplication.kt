package com.pantip.prefstate.example

import android.app.Application
import com.pantip.android.prefstate.PrefState
import com.pantip.android.realmstate.RealmState
import com.peerapongsam.android.state.room.RoomState
import io.realm.Realm

/**
 * @author peerapongsam
 */
class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // SharedPreferences
        PrefState.initialize(applicationContext)

        // Realm
        Realm.init(applicationContext)
        RealmState.initialize(applicationContext)

        // Room
        RoomState.initialize(applicationContext)
    }

    override fun onTerminate() {
        super.onTerminate()

        // SharedPreferences
        PrefState.clearAll(applicationContext)

        // Realm
        RealmState.clearAll(applicationContext)

        // Room
        RoomState.clearAll(applicationContext)
    }
}