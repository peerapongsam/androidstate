package com.pantip.android.realmstate

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class RealmStateModule
