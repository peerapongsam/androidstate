package com.peerapongsam.android.state.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface BundleDao {

    @Query("SELECT * FROM states WHERE id = :id LIMIT 1")
    fun getEntityById(id: String): BundleEntity?

    @Query("DELETE FROM states WHERE id = :id")
    fun deleteEntityById(id: String)

    @Query("DELETE FROM states")
    fun deleteAllEntities()

    @Insert
    fun insert(entity: BundleEntity)
}