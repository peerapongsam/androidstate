package com.peerapongsam.android.state.room

import android.content.Context
import android.os.Bundle

object RoomState {

    private var roomStateDelegate: RoomStateDelegate? = null

    private fun checkInitialization() {
        if (roomStateDelegate == null) {
            throw IllegalStateException(
                    "You must first call initialize before calling any other methods")
        }
    }

    fun clear(target: Any) {
        checkInitialization()
        roomStateDelegate!!.clear(target)
    }

    fun clearAll(context: Context) {
        val delegate = if (roomStateDelegate != null)
            roomStateDelegate
        else
            RoomStateDelegate(context)

        delegate?.clearAll()
    }

    fun initialize(context: Context) {
        roomStateDelegate = RoomStateDelegate(context)
    }

    fun restoreInstanceState(target: Any, state: Bundle?): Bundle? {
        checkInitialization()
        return roomStateDelegate?.restoreInstanceState(target, state)
    }

    fun saveInstanceState(target: Any, bundle: Bundle, outState: Bundle) {
        checkInitialization()
        roomStateDelegate?.saveInstanceState(target, bundle, outState)
    }
}