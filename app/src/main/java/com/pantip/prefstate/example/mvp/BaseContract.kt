package com.pantip.prefstate.example.mvp

/**
 * @author peerapongsam
 */
interface BaseContract {
    interface View<in VM> {
        fun onViewModelChanged(viewModel: VM)
    }

    interface Presenter<VM, in V : View<VM>> {
        val currentViewModel: VM

        fun attachView(view: V)

        fun detachView()

        fun restoreViewModel(viewModel: VM)
    }
}